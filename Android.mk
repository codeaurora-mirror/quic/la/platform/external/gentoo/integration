LOCAL_PATH := $(call my-dir)

# Make sure they don't try putting quotes into the string.
# We'll take care of that for them.
ifneq (,$(findstring ',$(PRODUCT_3RD_PARTY_PACKAGES))$(findstring ",$(PRODUCT_3RD_PARTY_PACKAGES)))
$(error Please do not quote atoms in PRODUCT_3RD_PARTY_PACKAGES.)
endif

ifeq ($(TARGET_IS_64_BIT),true)
3RD_PARTY_LIBDIR := lib64
else
3RD_PARTY_LIBDIR := lib
endif

3RD_PARTY_CC := $(HOST_OUT_EXECUTABLES)/3rd-party-gcc
3RD_PARTY_CXX := $(HOST_OUT_EXECUTABLES)/3rd-party-g++
3RD_PARTY_PKG_CONFIG := $(HOST_OUT_EXECUTABLES)/3rd-party-pkg-config

TARGET_OUT_GENTOO := $(TARGET_OUT_INTERMEDIATES)/gentoo
TARGET_OUT_COMMON_GENTOO := $(TARGET_OUT_COMMON_INTERMEDIATES)/gentoo
3RD_PARTY_ROOT := $(TARGET_OUT_GENTOO)/root
3RD_PARTY_ROOT_SUBDIR := $(patsubst $(PRODUCT_OUT)/%,%,$(3RD_PARTY_ROOT))

# These are system packages that we assume are always available before we can
# compile anything at all.  Basically we need a functional C/C++ compiler.
3RD_PARTY_SYSTEM_DEPS := libc libc++ libdl libm libstdc++

# We have to include subdirs first as they set up some vars we use below.
3RD_PARTY_CONFIGS :=
3RD_PARTY_WRAPPERS :=
include $(call all-subdir-makefiles)

# Some libraries are available in external/ already.
# Note: Keep this in sync with gentoo/integration/portage/package.provided.in.
# We do not list packages that do not provide libs/headers.
3RD_PARTY_EXTERNAL_LIBS := \
	libavahi-client libavahi-common libavahi-core \
	libbz \
	libcrypto libssl \
	libcares \
	libcurl \
	libdbus \
	libext2_blkid libext2_com_err libext2_e2p libext2fs libext2_quota libext2_uuid libtune2fs \
	libdw libdwelf libdwfl libebl libelf \
	libexpat \
	libFLAC \
	libgmock \
	libgtest \
	libicui18n libicuuc \
	libiprouteutil libnetlink \
	libip4tc libip6tc libxtables \
	libjemalloc \
	libjpeg \
	libcap \
	libcap-ng \
	libdaemon \
	libdivsufsort libdivsufsort64 \
	libevent \
	libmicrohttpd \
	libnl \
	libogg \
	libopus \
	libselinux \
	libunwind \
	libvpx libwebm \
	libpcre libpcrecpp \
	libsepol \
	libspeex \
	libtinyxml \
	libtinyxml2 \
	libwpa_client \
	libz \
	$(NULL)
# Only depend on the packages that are actually requested.
3RD_PARTY_EXTERNAL_DEPS += $(filter $(3RD_PARTY_EXTERNAL_LIBS),$(product_MODULES) $(PRODUCT_PACKAGES))

# A pseudo target that depends on all the generated packages above.
# The board can use this in their PRODUCT_PACKAGES list.
include $(CLEAR_VARS)
LOCAL_MODULE := 3rd-party-packages
LOCAL_MODULE_CLASS := GENTOO
LOCAL_ADDITIONAL_DEPENDENCIES := $(3RD_PARTY_WRAPPERS) $(3RD_PARTY_CONFIGS) $(3RD_PARTY_EXTERNAL_DEPS)

include $(BUILD_SYSTEM)/base_rules.mk

3RD_PARTY_PACKAGES_QUOTED := $(patsubst %,'%',$(PRODUCT_3RD_PARTY_PACKAGES))
LOCAL_BUILT_MODULE := $(intermediates)/$(LOCAL_MODULE)
# The ANDROID_xxx vars are recreated like envsetup.sh does.  They are used in
# cases like `make PRODUCT-<product>-eng` which doesn't source envsetup.sh.
$(LOCAL_BUILT_MODULE): $(LOCAL_ADDITIONAL_DEPENDENCIES)
	$(hide)mkdir -p $(dir $@) $(TARGET_OUT_GENTOO)/tmp
ifneq ($(PRODUCT_3RD_PARTY_PACKAGES),)
	$(hide)\
		printf '%s\n' $(3RD_PARTY_PACKAGES_QUOTED) | LC_ALL=C sort > $@.tmp; \
		if cmp $@.tmp $@ 2>/dev/null; then \
			mv $@.tmp $@; \
			exit 0; \
		fi; \
		export ANDROID_TOOLCHAIN="$(if $(filter /%,$(TARGET_TOOLCHAIN_ROOT)),,$(PWD)/)$(TARGET_TOOLCHAIN_ROOT)/bin"; \
		export ANDROID_BUILD_PATHS="$(if $(filter /%,$(HOST_OUT_EXECUTABLES)),,$(PWD)/)$(HOST_OUT_EXECUTABLES):$${ANDROID_TOOLCHAIN}:$(ANDROID_BUILD_PATHS)"; \
		export ANDROID_PRODUCT_OUT="$(if $(filter /%,$(PRODUCT_OUT)),,$(PWD)/)$(PRODUCT_OUT)"; \
		export ANDROID_BUILD_TOP="$(PWD)"; \
		$(HOST_OUT_EXECUTABLES)/emerge -uN $(3RD_PARTY_PACKAGES_QUOTED) || exit; \
		$(HOST_OUT_EXECUTABLES)/3rd-party-merge \
			$(patsubst %,--package %,$(3RD_PARTY_PACKAGES_QUOTED)) \
			--input-root $(3RD_PARTY_ROOT) \
			--output-root $(TARGET_OUT) \
			--make-root $(TARGET_OUT_GENTOO)/mk-installed
endif
	$(hide)touch $@
